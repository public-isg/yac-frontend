FROM node:22.3.0-alpine3.20 as build
WORKDIR /app
COPY ./app /app/
RUN apk add --update python3 make g++ \
   && rm -rf /var/cache/apk/*
RUN npm ci
RUN npm run build

FROM nginx:1.27.0-alpine as production
WORKDIR /usr/share/nginx/html
COPY --from=build /app/dist .
COPY ./app/public .
EXPOSE 80
CMD ["sh", "-c", "envsubst < config.json > config.tmp.json && mv config.tmp.json config.json && nginx -g 'daemon off;'"]
