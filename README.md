# YAC Frontend

YAC (Yet Another Configurator) is a highly adjustable tool to modify YAML files
in a GIT repository via UI/API. It allows versatile permission definitions on
file and parameter/value level.

For the backend API, see: https://gitlab.inf.ethz.ch/public-isg/yac-backend

## Deployment

    docker run --rm --name yac-frontend -p 80:80 \
        --env YAC_NAME='My YAC Instance' \
        --env YAC_BACKEND_URL='https://api.yac.example.com' \
        yac-frontend:latest

## Development

### Upgrade Environment

- Check on https://hub.docker.com/_/node and https://hub.docker.com/_/nginx for
  new versions and adjust the tag in the `FROM` instructions of `./Dockerfile`.
  (Use a most specific tag to allow reproducable builds.)

- Generate a new package-lock file with:

      docker run --rm --mount type=bind,source=$(pwd)/app,target=/app \
          node:current-alpine sh -c "cd /app; npm install --package-lock-only"
